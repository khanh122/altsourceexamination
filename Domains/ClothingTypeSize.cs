﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domains
{
    public class ClothingTypeSize
    {
        public long ClothingTypeId { get; set; }
        public long ClothingSizeId { get; set; }
    }
}
