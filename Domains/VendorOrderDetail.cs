﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domains
{
    public class VendorOrderDetail
    {
        public long VendorOrderId { get; set; }
        public long ClothingId { get; set; }
        public long Quantity { get; set; }
        public double SellRate { get; set; }
        public double BuyRate { get; set; }
    }
}
