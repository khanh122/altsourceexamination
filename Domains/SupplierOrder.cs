﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domains
{
    public class SupplierOrder
    {
        public long SupplierOrderId { get; set; }
        public long SupplierID { get; set; }
        public long VendorId { get; set; }
        public double TotalPrice { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
