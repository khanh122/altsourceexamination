﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domains
{
    public class VendorOrder
    {
        public long VendorOrderId { get; set; }
        public long VendorId { get; set; }
        public long CustomerId { get; set; }
        public long TransactionTypeId { get; set; }
        public double TotalSellRate { get; set; }
        public double TotalBuyRate { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
