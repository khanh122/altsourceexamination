﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domains
{
    public class ClothingColour
    {
        public long ClothingColourId { get; set; }
        public string ClothingColourName { get; set; }
    }
}
