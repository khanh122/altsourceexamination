﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domains
{
    public class Vendor
    {
        public long VendorId { get; set; }
        public string VendorName { get; set; }
    }
}
