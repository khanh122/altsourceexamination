﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domains
{
    public class Customer
    {
        public long CustomerId { get; set; }
        public string CustomerName { get; set; }
    }
}
