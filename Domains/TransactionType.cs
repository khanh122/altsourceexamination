﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domains
{
    public class TransactionType
    {
        public long TransactionId { get; set; }
        public string TransactionName { get; set; }
    }
}
