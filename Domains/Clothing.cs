﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domains
{
    public class Clothing
    {
        public long ClothingId { get; set; }
        public long ClothingTypeId { get; set; }

        public string ClothingName { get; set; }
        public double SellRate { get; set; }
        public double BuyRate { get; set; }
    }
}
