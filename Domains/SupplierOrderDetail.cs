﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domains
{
    public class SupplierOrderDetail
    {
        public long ClothingId { get; set; }
        public long SupplierOrderId { get; set; }
        public long Quantity { get; set; }
    }
}
