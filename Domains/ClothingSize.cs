﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domains
{
    public class ClothingSize
    {
        public long ClothingSizeId { get; set; }
        public string ClothingSizeName { get; set; }
    }
}
