﻿using InMemoryRepos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShirtForm
{
    public partial class ClothingTypeFrm : Form
    {
        public ClothingTypeFrm()
        {
            InitializeComponent();
        }

        private void LoadClothingType()
        {
            var clothingTypes = ClothingTypeRepository.GetClothingTypes();
            grClothingType.DataSource = clothingTypes.ToList();
        }

        private void ClothingTypeFrm_Load(object sender, EventArgs e)
        {
            LoadClothingType();
        }

        private void BtAddClothingType_Click(object sender, EventArgs e)
        {
            try
            {
                var clothingTypeName = txtTypeName.Text;

                ClothingTypeRepository.AddClothingType(clothingTypeName);
                LoadClothingType();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "error");
            }
        }
    }
}
