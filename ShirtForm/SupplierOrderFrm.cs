﻿using Domains;
using InMemoryRepos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShirtForm
{
    public partial class SupplierOrderFrm : Form
    {
        public SupplierOrderFrm()
        {
            InitializeComponent();
        }

        private void LoadSupplierCombobox()
        {
            var suppliers = SupplierRepository.GetSuppliers();
            cbSupplier.DataSource = suppliers;
            cbSupplier.DisplayMember = "SupplierName";
            cbSupplier.ValueMember = "SupplierId";
        }

        private void LoadVendorCombobox()
        {
            var vendors = VendorRepository.GetVendors();
            cbVendor.DataSource = vendors;
            cbVendor.DisplayMember = "VendorName";
            cbVendor.ValueMember = "VendorId";
        }

        private void LoadClothesGridView()
        {
            grClothes.DataSource = ClothingRepository.GetClothings().ToList();

        }

        private void SupplierOrderFrm_Load(object sender, EventArgs e)
        {
            LoadSupplierCombobox();
            LoadVendorCombobox();
            LoadClothesGridView();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedVendor = cbVendor.SelectedItem as Vendor;
                var selectedSupplier = cbSupplier.SelectedItem as Supplier;
                var selectedClothes = grClothes.SelectedRows;

                if (selectedVendor != null && selectedSupplier != null && selectedClothes.Count > 0)
                {
                    var supplierOrderId = SupplierOrderRepository.GetNextSequenceNumber();

                    var supplierOrder = new SupplierOrder
                    {
                        SupplierOrderId = supplierOrderId,
                        OrderDate = DateTime.Now,
                        SupplierID = selectedSupplier.SupplierId,
                        VendorId = selectedVendor.VendorId
                    };

                    SupplierOrderRepository.AddSupplierOrder(supplierOrder);

                    for (int i = 0; i < grClothes.SelectedRows.Count; i++)
                    {
                        string id = grClothes.SelectedCells[i].Value.ToString();

                        long clothingId = long.Parse(id);

                        var supplierOrderDetail = new SupplierOrderDetail
                        {
                            ClothingId = clothingId,
                            SupplierOrderId = supplierOrderId
                        };

                        SupplierOrderDetailRepository.AddSupplierOrderDetail(supplierOrderDetail);
                    }

                    MessageBox.Show("Add Supplier order successfully", "Successfull");


                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "error");
            }
        }
    }
}
