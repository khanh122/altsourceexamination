﻿namespace ShirtForm
{
    partial class ClothingTypeFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.txtTypeName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btAddClothingType = new System.Windows.Forms.Button();
            this.grClothingType = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.grClothingType)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "TypeName";
            // 
            // txtTypeName
            // 
            this.txtTypeName.Location = new System.Drawing.Point(77, 19);
            this.txtTypeName.Name = "txtTypeName";
            this.txtTypeName.Size = new System.Drawing.Size(100, 20);
            this.txtTypeName.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Select Colour";
            // 
            // btAddClothingType
            // 
            this.btAddClothingType.Location = new System.Drawing.Point(382, 253);
            this.btAddClothingType.Name = "btAddClothingType";
            this.btAddClothingType.Size = new System.Drawing.Size(111, 23);
            this.btAddClothingType.TabIndex = 10;
            this.btAddClothingType.Text = "Add New Clothing Type";
            this.btAddClothingType.UseVisualStyleBackColor = true;
            this.btAddClothingType.Click += new System.EventHandler(this.BtAddClothingType_Click);
            // 
            // grClothingType
            // 
            this.grClothingType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grClothingType.Location = new System.Drawing.Point(15, 83);
            this.grClothingType.Name = "grClothingType";
            this.grClothingType.Size = new System.Drawing.Size(478, 150);
            this.grClothingType.TabIndex = 11;
            // 
            // ClothingTypeFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 378);
            this.Controls.Add(this.grClothingType);
            this.Controls.Add(this.btAddClothingType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtTypeName);
            this.Controls.Add(this.label3);
            this.Name = "ClothingTypeFrm";
            this.Text = "ClothingTypeFrm";
            this.Load += new System.EventHandler(this.ClothingTypeFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grClothingType)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTypeName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btAddClothingType;
        private System.Windows.Forms.DataGridView grClothingType;
    }
}