﻿using InMemoryRepos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShirtForm
{
    public partial class SupplierOrderDetailFrm : Form
    {
        public SupplierOrderDetailFrm()
        {
            InitializeComponent();
        }

        private void LoadSupplierOrderDetailForm()
        {
            grSupplierOrderDetail.DataSource = SupplierOrderDetailRepository.GetSupplierOrderDetails().ToList();
        }

        private void SupplierOrderDetailFrm_Load(object sender, EventArgs e)
        {
            LoadSupplierOrderDetailForm();
        }
    }
}
