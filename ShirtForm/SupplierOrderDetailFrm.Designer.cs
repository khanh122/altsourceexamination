﻿namespace ShirtForm
{
    partial class SupplierOrderDetailFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grSupplierOrderDetail = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.grSupplierOrderDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // grSupplierOrderDetail
            // 
            this.grSupplierOrderDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grSupplierOrderDetail.Location = new System.Drawing.Point(12, 27);
            this.grSupplierOrderDetail.Name = "grSupplierOrderDetail";
            this.grSupplierOrderDetail.Size = new System.Drawing.Size(751, 241);
            this.grSupplierOrderDetail.TabIndex = 0;
            // 
            // SupplierOrderDetailFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.grSupplierOrderDetail);
            this.Name = "SupplierOrderDetailFrm";
            this.Text = "SupplierOrderDetailFrm";
            this.Load += new System.EventHandler(this.SupplierOrderDetailFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grSupplierOrderDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grSupplierOrderDetail;
    }
}