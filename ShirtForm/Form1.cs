﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShirtForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ClothingTypeFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClothingTypeFrm clothingTypeFrm = new ClothingTypeFrm();
            clothingTypeFrm.Show();
        }

        private void AddClothingTypeColourToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClothingTypeColourFrm clothingTypeColourFrm = new ClothingTypeColourFrm();
            clothingTypeColourFrm.Show();
        }

        private void AddClothingTypeSizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClothingTypeSizeFrm clothingTypeSizeFrm = new ClothingTypeSizeFrm();
            clothingTypeSizeFrm.Show();
        }

        private void VendorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VendorForm vendorForm = new VendorForm();
            vendorForm.Show();
        }

        private void SupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SupplierForm supplierForm = new SupplierForm();
            supplierForm.Show();
        }

        private void ClothingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClothingForm clothingForm = new ClothingForm();
            clothingForm.Show();
        }

        private void VendorOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VedorOrderFrm vedorOrderFrm = new VedorOrderFrm();
            vedorOrderFrm.Show();
        }

        private void SupplierToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SupplierOrderFrm supplierOrderFrm = new SupplierOrderFrm();
            supplierOrderFrm.Show();
        }

        private void ClothingTypeSizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClothingTypeSizeFrm clothingTypeSizeFrm = new ClothingTypeSizeFrm();
            clothingTypeSizeFrm.Show();
        }

        private void ClothingTypeColourToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClothingTypeColourFrm clothingTypeColourFrm = new ClothingTypeColourFrm();
            clothingTypeColourFrm.Show();
        }

        private void SupplierOrderDetailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SupplierOrderDetailFrm supplierOrderDetailFrm = new SupplierOrderDetailFrm();
            supplierOrderDetailFrm.Show();
        }

        private void VendorOrderDetailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VendorOrderDetailFrm vendorOrderDetailFrm = new VendorOrderDetailFrm();
            vendorOrderDetailFrm.Show();
        }
    }
}
