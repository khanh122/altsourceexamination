﻿namespace ShirtForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.clothingTypeFormToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addClothingTypeColourToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addClothingTypeSizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clothingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.clothingTypeSizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clothingTypeColourToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierOrderDetailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorOrderDetailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clothingTypeFormToolStripMenuItem,
            this.vendorToolStripMenuItem,
            this.supplierToolStripMenuItem,
            this.clothingToolStripMenuItem,
            this.vendorOrderToolStripMenuItem,
            this.supplierToolStripMenuItem1,
            this.clothingTypeSizeToolStripMenuItem,
            this.clothingTypeColourToolStripMenuItem,
            this.supplierOrderDetailToolStripMenuItem,
            this.vendorOrderDetailToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1035, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // clothingTypeFormToolStripMenuItem
            // 
            this.clothingTypeFormToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addClothingTypeColourToolStripMenuItem,
            this.addClothingTypeSizeToolStripMenuItem});
            this.clothingTypeFormToolStripMenuItem.Name = "clothingTypeFormToolStripMenuItem";
            this.clothingTypeFormToolStripMenuItem.Size = new System.Drawing.Size(118, 20);
            this.clothingTypeFormToolStripMenuItem.Text = "ClothingTypeForm";
            this.clothingTypeFormToolStripMenuItem.Click += new System.EventHandler(this.ClothingTypeFormToolStripMenuItem_Click);
            // 
            // addClothingTypeColourToolStripMenuItem
            // 
            this.addClothingTypeColourToolStripMenuItem.Name = "addClothingTypeColourToolStripMenuItem";
            this.addClothingTypeColourToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.addClothingTypeColourToolStripMenuItem.Text = "Add Clothing Type Colour";
            this.addClothingTypeColourToolStripMenuItem.Click += new System.EventHandler(this.AddClothingTypeColourToolStripMenuItem_Click);
            // 
            // addClothingTypeSizeToolStripMenuItem
            // 
            this.addClothingTypeSizeToolStripMenuItem.Name = "addClothingTypeSizeToolStripMenuItem";
            this.addClothingTypeSizeToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.addClothingTypeSizeToolStripMenuItem.Text = "Add Clothing Type Size";
            this.addClothingTypeSizeToolStripMenuItem.Click += new System.EventHandler(this.AddClothingTypeSizeToolStripMenuItem_Click);
            // 
            // vendorToolStripMenuItem
            // 
            this.vendorToolStripMenuItem.Name = "vendorToolStripMenuItem";
            this.vendorToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.vendorToolStripMenuItem.Text = "Vendor";
            this.vendorToolStripMenuItem.Click += new System.EventHandler(this.VendorToolStripMenuItem_Click);
            // 
            // supplierToolStripMenuItem
            // 
            this.supplierToolStripMenuItem.Name = "supplierToolStripMenuItem";
            this.supplierToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.supplierToolStripMenuItem.Text = "Supplier";
            this.supplierToolStripMenuItem.Click += new System.EventHandler(this.SupplierToolStripMenuItem_Click);
            // 
            // clothingToolStripMenuItem
            // 
            this.clothingToolStripMenuItem.Name = "clothingToolStripMenuItem";
            this.clothingToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.clothingToolStripMenuItem.Text = "Clothing";
            this.clothingToolStripMenuItem.Click += new System.EventHandler(this.ClothingToolStripMenuItem_Click);
            // 
            // vendorOrderToolStripMenuItem
            // 
            this.vendorOrderToolStripMenuItem.Name = "vendorOrderToolStripMenuItem";
            this.vendorOrderToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.vendorOrderToolStripMenuItem.Text = "Vendor Order";
            this.vendorOrderToolStripMenuItem.Click += new System.EventHandler(this.VendorOrderToolStripMenuItem_Click);
            // 
            // supplierToolStripMenuItem1
            // 
            this.supplierToolStripMenuItem1.Name = "supplierToolStripMenuItem1";
            this.supplierToolStripMenuItem1.Size = new System.Drawing.Size(95, 20);
            this.supplierToolStripMenuItem1.Text = "Supplier Order";
            this.supplierToolStripMenuItem1.Click += new System.EventHandler(this.SupplierToolStripMenuItem1_Click);
            // 
            // clothingTypeSizeToolStripMenuItem
            // 
            this.clothingTypeSizeToolStripMenuItem.Name = "clothingTypeSizeToolStripMenuItem";
            this.clothingTypeSizeToolStripMenuItem.Size = new System.Drawing.Size(116, 20);
            this.clothingTypeSizeToolStripMenuItem.Text = "Clothing Type Size";
            this.clothingTypeSizeToolStripMenuItem.Click += new System.EventHandler(this.ClothingTypeSizeToolStripMenuItem_Click);
            // 
            // clothingTypeColourToolStripMenuItem
            // 
            this.clothingTypeColourToolStripMenuItem.Name = "clothingTypeColourToolStripMenuItem";
            this.clothingTypeColourToolStripMenuItem.Size = new System.Drawing.Size(132, 20);
            this.clothingTypeColourToolStripMenuItem.Text = "Clothing Type Colour";
            this.clothingTypeColourToolStripMenuItem.Click += new System.EventHandler(this.ClothingTypeColourToolStripMenuItem_Click);
            // 
            // supplierOrderDetailToolStripMenuItem
            // 
            this.supplierOrderDetailToolStripMenuItem.Name = "supplierOrderDetailToolStripMenuItem";
            this.supplierOrderDetailToolStripMenuItem.Size = new System.Drawing.Size(128, 20);
            this.supplierOrderDetailToolStripMenuItem.Text = "Supplier Order Detail";
            this.supplierOrderDetailToolStripMenuItem.Click += new System.EventHandler(this.SupplierOrderDetailToolStripMenuItem_Click);
            // 
            // vendorOrderDetailToolStripMenuItem
            // 
            this.vendorOrderDetailToolStripMenuItem.Name = "vendorOrderDetailToolStripMenuItem";
            this.vendorOrderDetailToolStripMenuItem.Size = new System.Drawing.Size(122, 20);
            this.vendorOrderDetailToolStripMenuItem.Text = "Vendor Order Detail";
            this.vendorOrderDetailToolStripMenuItem.Click += new System.EventHandler(this.VendorOrderDetailToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1035, 450);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem clothingTypeFormToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addClothingTypeColourToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addClothingTypeSizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clothingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem clothingTypeSizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clothingTypeColourToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierOrderDetailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorOrderDetailToolStripMenuItem;
    }
}

