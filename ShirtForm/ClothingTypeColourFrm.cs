﻿using Domains;
using InMemoryRepos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShirtForm
{
    public partial class ClothingTypeColourFrm : Form
    {
        public ClothingTypeColourFrm()
        {
            InitializeComponent();
        }

        private void LoadClothingTypeCombobox()
        {
            var clothingTypes = ClothingTypeRepository.GetClothingTypes();
            cbClothingTypes.DataSource = clothingTypes;
            cbClothingTypes.DisplayMember = "ClothingTypeName";
            cbClothingTypes.ValueMember = "ClothingTypeId";
        }

        private void LoadClothingTypeColour()
        {
            grClothingTypeColour.DataSource = ClothingTypeColourRepository.GetClothingTypeColours().ToList();
        }

        private void LoadClothingColourCombobox()
        {
            var clothingColours = ClothingColourRepository.GetClothingColours();
            cbClothingColour.DataSource = clothingColours;
            cbClothingColour.DisplayMember = "ClothingColourName";
            cbClothingColour.ValueMember = "ClothingColourId";
        }

        private void ClothingTypeColourFrm_Load(object sender, EventArgs e)
        {
            LoadClothingTypeCombobox();
            LoadClothingColourCombobox();
            LoadClothingTypeColour();
        }

        private void BtAddClothingTypeColour_Click(object sender, EventArgs e)
        {
            var selectedColour = cbClothingColour.SelectedItem as ClothingColour;
            var selectedType = cbClothingTypes.SelectedItem as ClothingType;

            if (null != selectedColour && null != selectedType)
            {
                long colourId = selectedColour.ClothingColourId;
                long typeId = selectedType.ClothingTypeId;

                ClothingTypeColourRepository.AddClothingTypeColour(typeId, colourId);
                LoadClothingTypeColour();
            }
        }
    }
}
