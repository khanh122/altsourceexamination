﻿using InMemoryRepos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShirtForm
{
    public partial class VendorOrderDetailFrm : Form
    {
        public VendorOrderDetailFrm()
        {
            InitializeComponent();
        }

        private void LoadVendorOrderDetailGrid()
        {
            grVendorOrderDetail.DataSource = VendorOrderDetailRepository.GetVendorOrderDetails().ToList();
        }

        private void VendorOrderDetailFrm_Load(object sender, EventArgs e)
        {
            LoadVendorOrderDetailGrid();
        }
    }
}
