﻿using InMemoryRepos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShirtForm
{
    public partial class SupplierForm : Form
    {
        public SupplierForm()
        {
            InitializeComponent();
        }

        private void LoadSupplierGridView()
        {
            grSupplier.DataSource = SupplierRepository.GetSuppliers().ToList();
        }

        private void BtnAddSupplier_Click(object sender, EventArgs e)
        {

            try
            {
                string supplierName = txtSupplierName.Text;
                SupplierRepository.AddSupplier(supplierName);
                LoadSupplierGridView();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void SupplierForm_Load(object sender, EventArgs e)
        {
            LoadSupplierGridView();
        }
    }
}
