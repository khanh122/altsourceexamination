﻿namespace ShirtForm
{
    partial class ClothingTypeSizeFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbClothingType = new System.Windows.Forms.ComboBox();
            this.cbClothingSize = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.grClothingTypeSize = new System.Windows.Forms.DataGridView();
            this.btAddClothingTypeSize = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grClothingTypeSize)).BeginInit();
            this.SuspendLayout();
            // 
            // cbClothingType
            // 
            this.cbClothingType.FormattingEnabled = true;
            this.cbClothingType.Location = new System.Drawing.Point(90, 45);
            this.cbClothingType.Name = "cbClothingType";
            this.cbClothingType.Size = new System.Drawing.Size(121, 21);
            this.cbClothingType.TabIndex = 0;
            // 
            // cbClothingSize
            // 
            this.cbClothingSize.FormattingEnabled = true;
            this.cbClothingSize.Location = new System.Drawing.Point(328, 45);
            this.cbClothingSize.Name = "cbClothingSize";
            this.cbClothingSize.Size = new System.Drawing.Size(121, 21);
            this.cbClothingSize.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Clothing Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(230, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Clothing Size";
            // 
            // grClothingTypeSize
            // 
            this.grClothingTypeSize.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grClothingTypeSize.Location = new System.Drawing.Point(15, 121);
            this.grClothingTypeSize.Name = "grClothingTypeSize";
            this.grClothingTypeSize.Size = new System.Drawing.Size(434, 208);
            this.grClothingTypeSize.TabIndex = 4;
            // 
            // btAddClothingTypeSize
            // 
            this.btAddClothingTypeSize.Location = new System.Drawing.Point(311, 72);
            this.btAddClothingTypeSize.Name = "btAddClothingTypeSize";
            this.btAddClothingTypeSize.Size = new System.Drawing.Size(138, 23);
            this.btAddClothingTypeSize.TabIndex = 5;
            this.btAddClothingTypeSize.Text = "Add Clothing Type Size";
            this.btAddClothingTypeSize.UseVisualStyleBackColor = true;
            this.btAddClothingTypeSize.Click += new System.EventHandler(this.BtAddClothingTypeSize_Click);
            // 
            // ClothingTypeSizeFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 379);
            this.Controls.Add(this.btAddClothingTypeSize);
            this.Controls.Add(this.grClothingTypeSize);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbClothingSize);
            this.Controls.Add(this.cbClothingType);
            this.Name = "ClothingTypeSizeFrm";
            this.Text = "ClothingTypeSizeFrm";
            this.Load += new System.EventHandler(this.ClothingTypeSizeFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grClothingTypeSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbClothingType;
        private System.Windows.Forms.ComboBox cbClothingSize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView grClothingTypeSize;
        private System.Windows.Forms.Button btAddClothingTypeSize;
    }
}