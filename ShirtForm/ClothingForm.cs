﻿using Domains;
using InMemoryRepos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShirtForm
{
    public partial class ClothingForm : Form
    {
        public ClothingForm()
        {
            InitializeComponent();
        }

        private void LoadClothingGridView()
        {
            grClothing.DataSource = ClothingRepository.GetClothings().ToList();
        }

        private void LoadClothingTypeCombobox()
        {
            var clothingTypes = ClothingTypeRepository.GetClothingTypes();
            cbClothingType.DataSource = clothingTypes;
            cbClothingType.DisplayMember = "ClothingTypeName";
            cbClothingType.ValueMember = "ClothingTypeId";
        }


        private void ClothingForm_Load(object sender, EventArgs e)
        {
            LoadClothingTypeCombobox();
            LoadClothingGridView();
        }

        private void BtAddClothing_Click(object sender, EventArgs e)
        {
            try
            {
                string clothingName = txtClothingName.Text;

                var selectedClothingType = cbClothingType.SelectedItem as ClothingType;

                if (!string.IsNullOrEmpty(clothingName) && selectedClothingType != null)
                {
                    ClothingRepository.AddClothing(clothingName, selectedClothingType.ClothingTypeId);
                    LoadClothingGridView();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "error");
            }
        }
    }
}
