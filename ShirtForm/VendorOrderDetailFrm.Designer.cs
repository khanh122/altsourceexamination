﻿namespace ShirtForm
{
    partial class VendorOrderDetailFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grVendorOrderDetail = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.grVendorOrderDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // grVendorOrderDetail
            // 
            this.grVendorOrderDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grVendorOrderDetail.Location = new System.Drawing.Point(12, 54);
            this.grVendorOrderDetail.Name = "grVendorOrderDetail";
            this.grVendorOrderDetail.Size = new System.Drawing.Size(753, 282);
            this.grVendorOrderDetail.TabIndex = 0;
            // 
            // VendorOrderDetailFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.grVendorOrderDetail);
            this.Name = "VendorOrderDetailFrm";
            this.Text = "VendorOrderDetailFrm";
            this.Load += new System.EventHandler(this.VendorOrderDetailFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grVendorOrderDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grVendorOrderDetail;
    }
}