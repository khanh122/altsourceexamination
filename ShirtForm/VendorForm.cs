﻿using InMemoryRepos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShirtForm
{
    public partial class VendorForm : Form
    {
        public VendorForm()
        {
            InitializeComponent();
        }

        private void LoadVendorGridView()
        {
            grVendor.DataSource = VendorRepository.GetVendors().ToList();
        }


        private void BtnAddVendor_Click(object sender, EventArgs e)
        {
            try
            {
                var vendorName = txtVendorName.Text;
                VendorRepository.AddVendor(vendorName);
                LoadVendorGridView();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error");
            }
        }

        private void VendorForm_Load(object sender, EventArgs e)
        {
            LoadVendorGridView();
        }
    }
}
