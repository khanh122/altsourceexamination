﻿using Domains;
using InMemoryRepos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShirtForm
{
    public partial class VedorOrderFrm : Form
    {
        public VedorOrderFrm()
        {
            InitializeComponent();
        }

        private void LoadClothingGridView()
        {
            grClothing.DataSource = ClothingRepository.GetClothings().ToList();

        }

        private void LoadCustomerCombobox()
        {
            var customers = CustomerRepository.GetCustomers();
            cbCustomer.DataSource = customers;
            cbCustomer.DisplayMember = "CustomerName";
            cbCustomer.ValueMember = "CustomerId";
        }

        private void LoadVendorCombobox()
        {
            var vendors = VendorRepository.GetVendors();
            cbVendor.DataSource = vendors;
            cbVendor.DisplayMember = "VendorName";
            cbVendor.ValueMember = "VendorId";
        }

        private void LoadTransactionType()
        {
            var transactions = TransactionRepository.GetTransactionTypes();
            cbTransactionType.DataSource = transactions;
            cbTransactionType.DisplayMember = "TransactionName";
            cbTransactionType.ValueMember = "TransactionId";
        }

        private void VedorOrderFrm_Load(object sender, EventArgs e)
        {
            LoadVendorCombobox();
            LoadTransactionType();
            LoadCustomerCombobox();
            LoadClothingGridView();
        }

        private void BtCreateVendorOrder_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedVendor = cbVendor.SelectedItem as Vendor;
                var selectedCustomer = cbCustomer.SelectedItem as Customer;
                var selectedTransaction = cbTransactionType.SelectedItem as TransactionType;

                var selectedClothingRows = grClothing.SelectedRows;

                if (selectedVendor != null && selectedCustomer != null && selectedTransaction != null)
                {
                    long vendorOrderId = VendorOrderRepository.GetNextSequenceNumber();

                    var vendorOrder = new VendorOrder
                    {
                        VendorOrderId = vendorOrderId,
                        CustomerId = selectedCustomer.CustomerId,
                        TransactionTypeId = selectedTransaction.TransactionId,
                        OrderDate = DateTime.Now,
                        VendorId = selectedVendor.VendorId
                    };

                    VendorOrderRepository.AddVendorOrder(vendorOrder);

                    List<long> selectedClothingIds = new List<long>();

                    for (int i = 0; i < grClothing.SelectedRows.Count; i++)
                    {
                        string id = grClothing.SelectedCells[i].Value.ToString();
                        long clothingId = long.Parse(id);

                        var vendorOrderDetail = new VendorOrderDetail
                        {
                            ClothingId = clothingId,
                            VendorOrderId = vendorOrderId
                        };

                        VendorOrderDetailRepository.AddVendorOrderDetail(vendorOrderDetail);
                    }

                    MessageBox.Show("Add vendor order successfully", "successfull");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "error");
            }
        }
    }
}
