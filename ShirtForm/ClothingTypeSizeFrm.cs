﻿using Domains;
using InMemoryRepos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShirtForm
{
    public partial class ClothingTypeSizeFrm : Form
    {
        public ClothingTypeSizeFrm()
        {
            InitializeComponent();
        }

        private void LoadClothingSizeCombobox()
        {
            var clothingSize = ClothingSizeRepository.GetClothingSizes();
            cbClothingSize.DataSource = clothingSize;
            cbClothingSize.DisplayMember = "ClothingSizeName";
            cbClothingSize.ValueMember = "ClothingSizeId";
        }

        private void LoadClothingTypeSizeGrid()
        {
            grClothingTypeSize.DataSource = ClothingTypeSizeRepository.GetClothingTypeSizes().ToList();
        }


        private void LoadClothingTypeCombobox()
        {
            var clothingTypes = ClothingTypeRepository.GetClothingTypes();
            cbClothingType.DataSource = clothingTypes;
            cbClothingType.DisplayMember = "ClothingTypeName";
            cbClothingType.ValueMember = "ClothingTypeId";
        }

        private void ClothingTypeSizeFrm_Load(object sender, EventArgs e)
        {
            LoadClothingSizeCombobox();
            LoadClothingTypeCombobox();
            LoadClothingTypeSizeGrid();
        }

        private void BtAddClothingTypeSize_Click(object sender, EventArgs e)
        {
            var selectedType = cbClothingType.SelectedItem as ClothingType;
            var selectedSize = cbClothingSize.SelectedItem as ClothingSize;

            if (selectedType != null && selectedSize != null)
            {
                ClothingTypeSizeRepository.AddClothingTypeSize(selectedType.ClothingTypeId, selectedSize.ClothingSizeId);
                LoadClothingTypeSizeGrid();
            }
        }
    }
}
