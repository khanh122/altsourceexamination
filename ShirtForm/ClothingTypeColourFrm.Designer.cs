﻿namespace ShirtForm
{
    partial class ClothingTypeColourFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbClothingTypes = new System.Windows.Forms.ComboBox();
            this.cbClothingColour = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btAddClothingTypeColour = new System.Windows.Forms.Button();
            this.grClothingTypeColour = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.grClothingTypeColour)).BeginInit();
            this.SuspendLayout();
            // 
            // cbClothingTypes
            // 
            this.cbClothingTypes.FormattingEnabled = true;
            this.cbClothingTypes.Location = new System.Drawing.Point(104, 52);
            this.cbClothingTypes.Name = "cbClothingTypes";
            this.cbClothingTypes.Size = new System.Drawing.Size(121, 21);
            this.cbClothingTypes.TabIndex = 0;
            // 
            // cbClothingColour
            // 
            this.cbClothingColour.FormattingEnabled = true;
            this.cbClothingColour.Location = new System.Drawing.Point(348, 52);
            this.cbClothingColour.Name = "cbClothingColour";
            this.cbClothingColour.Size = new System.Drawing.Size(121, 21);
            this.cbClothingColour.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Clothing Type:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(251, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Clothing Colour:";
            // 
            // btAddClothingTypeColour
            // 
            this.btAddClothingTypeColour.Location = new System.Drawing.Point(348, 79);
            this.btAddClothingTypeColour.Name = "btAddClothingTypeColour";
            this.btAddClothingTypeColour.Size = new System.Drawing.Size(121, 23);
            this.btAddClothingTypeColour.TabIndex = 4;
            this.btAddClothingTypeColour.Text = "Add Clothing Colour";
            this.btAddClothingTypeColour.UseVisualStyleBackColor = true;
            this.btAddClothingTypeColour.Click += new System.EventHandler(this.BtAddClothingTypeColour_Click);
            // 
            // grClothingTypeColour
            // 
            this.grClothingTypeColour.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grClothingTypeColour.Location = new System.Drawing.Point(26, 123);
            this.grClothingTypeColour.Name = "grClothingTypeColour";
            this.grClothingTypeColour.Size = new System.Drawing.Size(443, 152);
            this.grClothingTypeColour.TabIndex = 5;
            // 
            // ClothingTypeColourFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 394);
            this.Controls.Add(this.grClothingTypeColour);
            this.Controls.Add(this.btAddClothingTypeColour);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbClothingColour);
            this.Controls.Add(this.cbClothingTypes);
            this.Name = "ClothingTypeColourFrm";
            this.Text = "ClothingTypeColourFrm";
            this.Load += new System.EventHandler(this.ClothingTypeColourFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grClothingTypeColour)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbClothingTypes;
        private System.Windows.Forms.ComboBox cbClothingColour;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btAddClothingTypeColour;
        private System.Windows.Forms.DataGridView grClothingTypeColour;
    }
}