﻿namespace ShirtForm
{
    partial class ClothingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtClothingName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbClothingType = new System.Windows.Forms.ComboBox();
            this.grClothing = new System.Windows.Forms.DataGridView();
            this.btAddClothing = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grClothing)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Clothing name";
            // 
            // txtClothingName
            // 
            this.txtClothingName.Location = new System.Drawing.Point(92, 25);
            this.txtClothingName.Name = "txtClothingName";
            this.txtClothingName.Size = new System.Drawing.Size(121, 20);
            this.txtClothingName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(219, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Clothing name";
            // 
            // cbClothingType
            // 
            this.cbClothingType.FormattingEnabled = true;
            this.cbClothingType.Location = new System.Drawing.Point(299, 25);
            this.cbClothingType.Name = "cbClothingType";
            this.cbClothingType.Size = new System.Drawing.Size(121, 21);
            this.cbClothingType.TabIndex = 3;
            // 
            // grClothing
            // 
            this.grClothing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grClothing.Location = new System.Drawing.Point(15, 113);
            this.grClothing.Name = "grClothing";
            this.grClothing.Size = new System.Drawing.Size(579, 214);
            this.grClothing.TabIndex = 4;
            // 
            // btAddClothing
            // 
            this.btAddClothing.Location = new System.Drawing.Point(426, 23);
            this.btAddClothing.Name = "btAddClothing";
            this.btAddClothing.Size = new System.Drawing.Size(75, 23);
            this.btAddClothing.TabIndex = 5;
            this.btAddClothing.Text = "Add Clothing";
            this.btAddClothing.UseVisualStyleBackColor = true;
            this.btAddClothing.Click += new System.EventHandler(this.BtAddClothing_Click);
            // 
            // ClothingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 374);
            this.Controls.Add(this.btAddClothing);
            this.Controls.Add(this.grClothing);
            this.Controls.Add(this.cbClothingType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtClothingName);
            this.Controls.Add(this.label1);
            this.Name = "ClothingForm";
            this.Text = "ClothingForm";
            this.Load += new System.EventHandler(this.ClothingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grClothing)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtClothingName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbClothingType;
        private System.Windows.Forms.DataGridView grClothing;
        private System.Windows.Forms.Button btAddClothing;
    }
}