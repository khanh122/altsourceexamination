﻿using Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepos
{
    public class VendorRepository
    {
        private static long nextVendorSequenceNumber = 5;
        private static List<Vendor> vendors = new List<Vendor>()
        {
             new Vendor { VendorId =1 , VendorName = "A" },
             new Vendor { VendorId =2 , VendorName = "B" },
             new Vendor { VendorId =3 , VendorName = "C" },
             new Vendor { VendorId =4 , VendorName = "D" },
        };

        public static void AddVendor(string vendorName)
        {
            var vendor = new Vendor
            {
                VendorId = nextVendorSequenceNumber++,
                VendorName = vendorName
            };

            vendors.Add(vendor);
        }

        public static IEnumerable<Vendor> GetVendors()
        {
            return vendors;
        }
    }
}
