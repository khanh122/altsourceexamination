﻿using Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepos
{
    public class SupplierOrderRepository
    {
        private static long nextSupplierOrderId = 1;
        private static List<SupplierOrder> supplierOrders = new List<SupplierOrder>();

        public static long GetNextSequenceNumber()
        {
            return nextSupplierOrderId++;
        }

        public static void AddSupplierOrder(SupplierOrder  supplierOrder)
        {
            if(null == supplierOrder)
            {
                throw new ArgumentNullException("supplier order can not be null");
            }

            supplierOrders.Add(supplierOrder);
        }
    }
}
