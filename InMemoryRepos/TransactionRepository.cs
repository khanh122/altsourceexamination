﻿using Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepos
{
    public class TransactionRepository
    {
        private static List<TransactionType> transactionTypes = new List<TransactionType>()
        {
             new TransactionType { TransactionId = 1, TransactionName ="Buy" },
             new TransactionType { TransactionId = 2, TransactionName ="Sell" }
        };

        public static IEnumerable<TransactionType> GetTransactionTypes()
        {
            return transactionTypes;
        }
    }
}
