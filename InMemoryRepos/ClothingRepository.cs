﻿using Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepos
{
    public class ClothingRepository
    {
        private static long nextClothingSequenceNumber = 4;
        private static List<Clothing> clothings = new List<Clothing>()
        {
            new Clothing  { ClothingId =1 , ClothingName ="T-Shirt", BuyRate = 6, SellRate = 12, ClothingTypeId = 1 },
            new Clothing  { ClothingId =2 , ClothingName ="Dress Shirt", BuyRate = 8, SellRate = 20, ClothingTypeId = 1 },
        };

        public static void AddClothing(string clothingName, long clothingTypeId)
        {
            var clothing = new Clothing
            {
                ClothingId = nextClothingSequenceNumber++,
                ClothingName = clothingName,
                ClothingTypeId = clothingTypeId
            };

            clothings.Add(clothing);

        }

        public static IEnumerable<Clothing> GetClothings()
        {
            return clothings;
        }
    }
}
