﻿using Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepos
{
    public class CustomerRepository
    {
        private static List<Customer> customers = new List<Customer>()
        {
             new Customer { CustomerId =1 , CustomerName ="AltSource" },
             new Customer { CustomerId =1 , CustomerName ="Git" },
             new Customer { CustomerId =1 , CustomerName ="SourceTree" }
        };

        public static IEnumerable<Customer> GetCustomers()
        {
            return customers;
        }
    }
}
