﻿using Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepos
{
    public class SupplierRepository
    {
        private static long nextSupplierSequenceNumber = 5;
        private static List<Supplier> suppliers = new List<Supplier>
        {
            new Supplier { SupplierId = 1, SupplierName = "Microsoft" },
            new Supplier { SupplierId = 2, SupplierName = "Google" },
            new Supplier { SupplierId = 3, SupplierName = "Apple" },
            new Supplier { SupplierId = 4, SupplierName = "Amazon" },
        };

        public static void AddSupplier(string supplierName)
        {
            var supplier = new Supplier
            {
                SupplierId = nextSupplierSequenceNumber++,
                SupplierName = supplierName
            };

            suppliers.Add(supplier);
        }

        public static IEnumerable<Supplier> GetSuppliers()
        {
            return suppliers;
        }
    }
}
