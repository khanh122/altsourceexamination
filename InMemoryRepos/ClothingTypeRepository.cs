﻿using Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepos
{
    public class ClothingTypeRepository
    {
        private static long nextSequenceNumber = 4;
        private static List<ClothingType> clothingTypes = new List<ClothingType>()
        {
            new ClothingType { ClothingTypeId = 1, ClothingTypeName = "Shirt" },
            new ClothingType { ClothingTypeId = 2, ClothingTypeName = "Dress" },
            new ClothingType { ClothingTypeId = 3, ClothingTypeName = "Vest" },
        };
            

        public static void AddClothingType(string clothingTypeName)
        {
            if (string.IsNullOrEmpty(clothingTypeName))
            {
                throw new ArgumentException(nameof(clothingTypeName) + " can not be null or empty value");
            }

            var clothingType = new ClothingType
            {
                ClothingTypeId = nextSequenceNumber++,
                ClothingTypeName = clothingTypeName
            };

            clothingTypes.Add(clothingType);
        }

        public static IEnumerable<ClothingType> GetClothingTypes()
        {
            return clothingTypes;
        }
    }
}
