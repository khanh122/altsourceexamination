﻿using Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepos
{
    public class VendorOrderRepository
    {
        private static long nextVendorOrderNumber = 1;
        private static List<VendorOrder> vendorOrders = new List<VendorOrder>();

        public static long GetNextSequenceNumber()
        {
            return nextVendorOrderNumber++;
        }

        public static void AddVendorOrder(VendorOrder vendorOrder)
        {
            vendorOrders.Add(vendorOrder);
        }

        public static IEnumerable<VendorOrder> GetVendorOrders()
        {
            return vendorOrders;
        }

    }
}
