﻿using Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepos
{
    public class ClothingSizeRepository
    {
        private static List<ClothingSize> clothingSizes = new List<ClothingSize>()
        {
             new ClothingSize { ClothingSizeId =1, ClothingSizeName ="L" },
             new ClothingSize { ClothingSizeId =2, ClothingSizeName ="L" },
             new ClothingSize { ClothingSizeId =3, ClothingSizeName ="L" },
             new ClothingSize { ClothingSizeId =4, ClothingSizeName ="L" },
             new ClothingSize { ClothingSizeId =5, ClothingSizeName ="XL" },
             new ClothingSize { ClothingSizeId =6, ClothingSizeName ="XXL" },
        };

        public static IEnumerable<ClothingSize> GetClothingSizes()
        {
            return clothingSizes;
        }
    }
}
