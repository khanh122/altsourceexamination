﻿using Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepos
{
    public class ClothingTypeSizeRepository
    {
        private static long nextSequenceNumber = 1;
        private static List<ClothingTypeSize> clothingTypeSizes = new List<ClothingTypeSize>();

        public static void AddClothingTypeSize(long clothingTypeId, long clothingSizeId)
        {
            var clothingTypeSize = new ClothingTypeSize
            {
                ClothingTypeId = clothingTypeId,
                ClothingSizeId = clothingSizeId
            };

            clothingTypeSizes.Add(clothingTypeSize);
        }

        public static IEnumerable<ClothingTypeSize> GetClothingTypeSizes()
        {
            return clothingTypeSizes;
        }

    }
}
