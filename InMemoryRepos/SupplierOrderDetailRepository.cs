﻿using Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepos
{
    public class SupplierOrderDetailRepository
    {
        private static List<SupplierOrderDetail> supplierOrderDetails = new List<SupplierOrderDetail>();

        public static void AddSupplierOrderDetail(SupplierOrderDetail supplierOrderDetail)
        {
            supplierOrderDetails.Add(supplierOrderDetail);
        }


        public static IEnumerable<SupplierOrderDetail> GetSupplierOrderDetails()
        {
            return supplierOrderDetails;
        }
    }
}
