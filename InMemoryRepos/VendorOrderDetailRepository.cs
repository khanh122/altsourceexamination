﻿using Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepos
{
    public class VendorOrderDetailRepository
    {
        private static List<VendorOrderDetail> vendorOrderDetails = new List<VendorOrderDetail>();

        public static void AddVendorOrderDetail(VendorOrderDetail vendorOrderDetail)
        {
            vendorOrderDetails.Add(vendorOrderDetail);
        }

        public static IEnumerable<VendorOrderDetail> GetVendorOrderDetails()
        {
            return vendorOrderDetails;
        }
    }
}
