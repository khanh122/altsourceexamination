﻿using Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepos
{
    public class ClothingColourRepository
    {
        private static List<ClothingColour> clothingColours = new List<ClothingColour>()
        {
            new ClothingColour{ ClothingColourId =1, ClothingColourName ="Blue" },
            new ClothingColour{ ClothingColourId =2, ClothingColourName ="Green" },
            new ClothingColour{ ClothingColourId =3, ClothingColourName ="Red" },
            new ClothingColour{ ClothingColourId =4, ClothingColourName ="Pink" },
            new ClothingColour{ ClothingColourId =5, ClothingColourName ="Purple" },
            new ClothingColour{ ClothingColourId =6, ClothingColourName ="Gray" }
        };

        public static IEnumerable<ClothingColour> GetClothingColours()
        {
            return clothingColours;
        }
    }
}
