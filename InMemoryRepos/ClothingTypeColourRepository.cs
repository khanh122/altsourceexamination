﻿using Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InMemoryRepos
{
    public class ClothingTypeColourRepository
    {
        private static List<ClothingTypeColour> clothingTypeColours = new List<ClothingTypeColour>();

        public static void AddClothingTypeColour(long clothingTypeId, long clothingColourId)
        {
            var clothingTypeColour = new ClothingTypeColour()
            {
                ClothingTypeId = clothingTypeId,
                ClothingTypeColourId = clothingColourId
            };

            clothingTypeColours.Add(clothingTypeColour);
        }

        public static IEnumerable<ClothingTypeColour> GetClothingTypeColours()
        {
            return clothingTypeColours;
        }
    }
}
